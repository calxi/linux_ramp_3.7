/*
 *  ramp_kbd.c
 *
 *  Copyright (c) 2011 Zhangxi Tan
 *
 */

/*
 * The ramp gold keyboard driver
 *
 */


#include <linux/module.h>
#include <linux/init.h>
#include <linux/input.h>
#include <linux/delay.h>
#include <linux/interrupt.h>

#include <asm/irq.h>
#include <asm/ramp.h>

#include "../arch/sparc/kernel/irq.h"

MODULE_AUTHOR("Zhangxi Tan <xtan@cs.berkeley.edu>");
MODULE_DESCRIPTION("RAMP Gold keyboard driver");
MODULE_LICENSE("BSD");

//supported keycode
static unsigned char rampkbd_keycode[] = {
    [0]	 = KEY_GRAVE,
    [1]	 = KEY_1,
    [2]	 = KEY_2,
    [3]	 = KEY_3,
    [4]	 = KEY_4,
    [5]	 = KEY_5,
    [6]	 = KEY_6,
    [7]	 = KEY_7,
    [8]	 = KEY_8,
    [9]	 = KEY_9,
    [10]	 = KEY_0,
    [11]	 = KEY_MINUS,
    [12]	 = KEY_EQUAL,
    [13]	 = KEY_BACKSLASH,
    [15]	 = KEY_KP0,
    [16]	 = KEY_Q,
    [17]	 = KEY_W,
    [18]	 = KEY_E,
    [19]	 = KEY_R,
    [20]	 = KEY_T,
    [21]	 = KEY_Y,
    [22]	 = KEY_U,
    [23]	 = KEY_I,
    [24]	 = KEY_O,
    [25]	 = KEY_P,
    [26]	 = KEY_LEFTBRACE,
    [27]	 = KEY_RIGHTBRACE,
    [29]	 = KEY_KP1,
    [30]	 = KEY_KP2,
    [31]	 = KEY_KP3,
    [32]	 = KEY_A,
    [33]	 = KEY_S,
    [34]	 = KEY_D,
    [35]	 = KEY_F,
    [36]	 = KEY_G,
    [37]	 = KEY_H,
    [38]	 = KEY_J,
    [39]	 = KEY_K,
    [40]	 = KEY_L,
    [41]	 = KEY_SEMICOLON,
    [42]	 = KEY_APOSTROPHE,
    [43]	 = KEY_BACKSLASH,
    [45]	 = KEY_KP4,
    [46]	 = KEY_KP5,
    [47]	 = KEY_KP6,
    [48]	 = KEY_102ND,
    [49]	 = KEY_Z,
    [50]	 = KEY_X,
    [51]	 = KEY_C,
    [52]	 = KEY_V,
    [53]	 = KEY_B,
    [54]	 = KEY_N,
    [55]	 = KEY_M,
    [56]	 = KEY_COMMA,
    [57]	 = KEY_DOT,
    [58]	 = KEY_SLASH,
    [60]	 = KEY_KPDOT,
    [61]	 = KEY_KP7,
    [62]	 = KEY_KP8,
    [63]	 = KEY_KP9,
    [64]	 = KEY_SPACE,
    [65]	 = KEY_BACKSPACE,
    [66]	 = KEY_TAB,
    [67]	 = KEY_KPENTER,
    [68]	 = KEY_ENTER,
    [69]	 = KEY_ESC,
    [70]	 = KEY_DELETE,
    [74]	 = KEY_KPMINUS,
    [76]	 = KEY_UP,
    [77]	 = KEY_DOWN,
    [78]	 = KEY_RIGHT,
    [79]	 = KEY_LEFT,
    [80]	 = KEY_F1,
    [81]	 = KEY_F2,
    [82]	 = KEY_F3,
    [83]	 = KEY_F4,
    [84]	 = KEY_F5,
    [85]	 = KEY_F6,
    [86]	 = KEY_F7,
    [87]	 = KEY_F8,
    [88]	 = KEY_F9,
    [89]	 = KEY_F10,
    [90]	 = KEY_KPLEFTPAREN,
    [91]	 = KEY_KPRIGHTPAREN,
    [92]	 = KEY_KPSLASH,
    [93]	 = KEY_KPASTERISK,
    [94]	 = KEY_KPPLUS,
    [95]	 = KEY_INSERT,
    [96]     = KEY_PAGEDOWN,
    [97]	 = KEY_PAGEUP,
    [98]	 = KEY_HOME,
    [99]	 = KEY_END,
    [100]	 = KEY_CAPSLOCK,
    [101]	 = KEY_LEFTSHIFT,
    [102]	 = KEY_RIGHTSHIFT,
    [103]	 = KEY_LEFTCTRL,
    [104]	 = KEY_LEFTALT,
    [105]	 = KEY_RIGHTALT
};

static struct input_dev *rampkbd_dev;

static irqreturn_t rampkbd_interrupt(int irq, void *data)
{
    struct input_dev *dev = data;
    unsigned int iodata;
    unsigned char scancode, down;

    //scancode goes here
    iodata = ramp_load_reg(RAMP_KEYBOARD_ADDR);
    scancode = iodata & 0xFF;
    down = iodata >> 31;

    iodata |= 1<<8;
    ramp_store_reg(RAMP_KEYBOARD_ADDR, iodata); //clear the data for new keycode

    // printk("ramp_kbd: %x, %x, %d\n", iodata, scancode, down);
    input_report_key(dev, scancode, down);

    input_sync(dev);

    return IRQ_HANDLED;
}


static int __init rampkbd_init(void)
{
    int i, irq, error;

    rampkbd_dev = input_allocate_device();
    if (!rampkbd_dev)
        return -ENOMEM;

    rampkbd_dev->name = "RAMP Keyboard";
    rampkbd_dev->phys = "rampkbd/input0";
    rampkbd_dev->id.bustype = BUS_VIRTUAL;
    rampkbd_dev->id.vendor = 0x0001;
    rampkbd_dev->id.product = 0x0001;
    rampkbd_dev->id.version = 0x0100;

    rampkbd_dev->evbit[0] = BIT_MASK(EV_KEY) | BIT_MASK(EV_REP);
    rampkbd_dev->keycode = rampkbd_keycode;
    rampkbd_dev->keycodesize = sizeof(unsigned char);
    rampkbd_dev->keycodemax = ARRAY_SIZE(rampkbd_keycode);

    for (i = 0; i < sizeof(rampkbd_keycode); i++) {
        set_bit(rampkbd_keycode[i], rampkbd_dev->keybit);
    }

    
    irq = sparc_config.build_device_irq(NULL, RAMP_KEYBOARD_INTNO);
    error = request_irq(irq, rampkbd_interrupt, 0, "rampkbd",
            rampkbd_dev);
    if (error) {
        printk("RAMP: keyboard cannot request irq @ %x, with err: %i \n",
                irq,
                error);
        goto fail;
    }

    ramp_store_reg(RAMP_KEYBOARD_ADDR, 1<<8); //clear the data for new keycode

    /* error check */
    error = input_register_device(rampkbd_dev);
    printk("RAMP: keyboard init failed to register device with err: %x\n", error);
    if (!error) 
        return 0;

fail:	input_free_device(rampkbd_dev);
        printk("RAMP: keyboard init failed");
        return error;
}

static void __exit rampkbd_exit(void)
{
    input_unregister_device(rampkbd_dev);
}

module_init(rampkbd_init);
module_exit(rampkbd_exit);
