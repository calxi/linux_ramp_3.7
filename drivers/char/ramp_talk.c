#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <asm/uaccess.h>    /* for put_user */

#include <linux/major.h>
#include <linux/vmalloc.h>
#include <linux/init.h>
#include <linux/blkdev.h>
#include <linux/bitops.h>
#include <linux/mutex.h>
#include <linux/slab.h>

#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/interrupt.h>
#include <linux/of_device.h>

#include <asm/setup.h>
#include <asm/pgtable.h>
#include <linux/spinlock.h>

static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

#define SUCCESS 0
#define DEVICE_NAME "ramptalk"   /* Dev name as it appears in /proc/devices   */
#define MAJOR_NUM 253
#define BUF_LEN 65536 /* Max length of the buffer, have to be word aligned*/

#define TOHOST_DIAB     0x81

/* 
 *  * Global variables are declared as static, so are global within the file. 
 *   */

static int ret;       /* Major number assigned to our device driver */
static char msg[BUF_LEN];/* The msg the device will give when asked */
static char *msg_Ptr = msg;
static volatile int *magic_mem;
/* memory layout in magic mem for ramp_talk
 * 10   Physical address of next byte written
 * 11   Physical address of last byte read
 * 12   Indicator for init: 1 for init, 0 otherwise
 * 13   Length of this circular buffer, set when init
 */

DEFINE_SPINLOCK(transfer_lock);

static struct file_operations fops = {
    .read = device_read,
    .write = device_write,
    .open = device_open,
    .release = device_release
};

/*
 *  * This function is called when the module is loaded
 *   */
static int talk_init_module(void)
{
    struct device_node *rootnp, *np;
    struct property *pp;
    int len, i;
    rootnp = of_find_node_by_path("/"); 

    if (rootnp) { 	
        np = of_find_node_by_name(rootnp, "ramp_cpu");
        if (np) {
            pp = of_find_property(np, "magic_mem", &len);
            if (pp) 
                magic_mem = (volatile int *)(*((unsigned int*)(pp->value)));		
        }
    }

    ret = register_chrdev(MAJOR_NUM, DEVICE_NAME, &fops);

    if (ret) {
        printk(KERN_ALERT "Registering char device failed with %d\n", MAJOR_NUM);
        return ret;
    }

    // init the buffer to be word-aligned
    // ret = posix_memalign(&msg, 4, BUF_LEN);
    // msg = kmalloc(BUF_LEN, GFP_KERNEL);
    // if (msg == NULL) {
    //     panic("ERROR: RAMP_TALK cannot allocate buffer.\n");
    //     return 0;
    // }

    // zero out the buffer
    for (i = 0; i < BUF_LEN; i++) {
        msg[i] = 0;    
    }
    // Transfer the first address to appserver
    // send to magic mem
    printk(KERN_INFO "RAMP_TALK: trying to initialize head pointer for appserver\n");

    msg_Ptr = (char*) ALIGN((int) msg_Ptr, 4);

    // init for communication
    magic_mem[10] = __pa(msg_Ptr);
    magic_mem[11] = __pa(msg_Ptr);
    magic_mem[12] = 1; // this is used in initialization to ensure appserver gets the first pointer
    magic_mem[13] = BUF_LEN;

    while (magic_mem[12] == 1)
        printk("waiting on addr %lu\n", __pa(magic_mem + 12));
    printk(KERN_INFO "RAMP_TALK established @ %lu \n", __pa(msg_Ptr));

    printk(KERN_INFO "I was assigned major number %d. To talk to\n", MAJOR_NUM);
    printk(KERN_INFO "the driver, create a dev file with\n");
    printk(KERN_INFO "'mknod /dev/%s c %d 0'.\n", DEVICE_NAME, MAJOR_NUM);

    return SUCCESS;
}

/*
 *  * This function is called when the module is unloaded
 *   */
static void talk_cleanup_module(void)
{
    unregister_chrdev(MAJOR_NUM, DEVICE_NAME);
}

module_init(talk_init_module);
module_exit(talk_cleanup_module);
/*
 *  * Methods
 *   */

/* 
 *  * Called when a process tries to open the device file, like
 *   * "cat /dev/mycharfile"
 *    */
static int device_open(struct inode *inode, struct file *file)
{
    try_module_get(THIS_MODULE);

    return SUCCESS;
}

/* 
 *  * Called when a process closes the device file.
 *   */
static int device_release(struct inode *inode, struct file *file)
{
    /* 
     *   * Decrement the usage count, or else once you opened the file, you'll
     *       * never get get rid of the module. 
     *           */
    module_put(THIS_MODULE);

    return 0;
}

/* 
 *  * Called when a process, which already opened the dev file, attempts to
 *   * read from it.
 *    */
static ssize_t device_read(struct file *filp,   /* see include/linux/fs.h   */
        char *buffer,    /* buffer to fill with data */
        size_t length,   /* length of the buffer     */
        loff_t * offset)
{
    /*
     *   * Number of bytes actually written to the buffer 
     *       */
    int bytes_read = 0;

    /* 
     *   * Actually put the data into the buffer 
     *       */
    while (length) {

        // put 0 to user since read isn't really supported
        put_user(0, buffer++);

        length--;
        bytes_read++;
    }
    printk(KERN_ALERT "read() operation isn't really supported.\n");

    /* 
     *   * Most read functions return the number of bytes put into the buffer
     *       */
    return bytes_read;
}

/*  
 *   * Called when a process writes to dev file: echo "hi" > /dev/hello 
 *    */
static ssize_t device_write(struct file *filp, const char *buff, size_t len, loff_t * off)
{
    unsigned long ret;
    int last_write, last_read, used_len, free_len = 0, len_to_end;
    spin_lock(&transfer_lock);

    //    ---Commented out is no circular buffer version--
    //    if (len + ( (int) msg_Ptr - (int) msg) >= BUF_LEN) {
    //        printk(KERN_ALERT "RAMP_TALK: run out of bufer\n");
    //        return 0;
    //    }
    //
    //    ret = copy_from_user(msg_Ptr, buff, len);
    //    if (ret > 0) {
    //        printk(KERN_ALERT "copy from user error, but whatever gets copied still will be transmitted.\n");
    //    }
    //
    //    // printk("RAMP_TALK receives msg: %s(EOF)\n", msg_Ptr);
    //    // printk("user-space buffer @ %x, len: %u, real len: %i\n", buff, len, ((int) &(msg_Ptr[len - ret])) - ((int) ALIGN((int) &(msg_Ptr[len - ret]), 4)));
    //
    //    msg_Ptr = &(msg_Ptr[len - ret]);
    //    msg_Ptr = (char*) ALIGN((int) msg_Ptr, 4);
    //    // put new pointer to it
    //    magic_mem[10] = __pa(msg_Ptr);

    if (len > BUF_LEN)
    {
        printk(KERN_ALERT "Attempting to write content that's larger than RAMP_TALK's maximum buffer. Aborting...\n");
        spin_unlock(&transfer_lock);
        return 0;
    }

    // First check if we should wait for the buffer to free up
    while (free_len <= len)
    {
        last_write = magic_mem[10];
        last_read  = magic_mem[11];
        used_len = last_write >= last_read ? last_write - last_read 
                                           : last_write + BUF_LEN - last_read;
        free_len = BUF_LEN - used_len;
        // printk("free len %i, write len: %i, last_write: %x, last_read: %x \n", free_len, len, __pa(last_write), __pa(last_read));
    }
    // printk("advancin with msg: %s\n", buff);

    // Now we can just write into buffer and update pointer, with the wrap-around trick
    len_to_end = BUF_LEN - (int) msg_Ptr + (int) msg;
    if (len > len_to_end)
    {
        ret = copy_from_user(msg_Ptr + 1, buff, len_to_end);
        if (ret > 0)
        {
            printk(KERN_ALERT "copy from user error(1), abort this write.\n");
            spin_unlock(&transfer_lock);
            return 0;
        }
        ret = copy_from_user(msg, buff + len_to_end, len - len_to_end);
        if (ret > 0)
        {
            printk(KERN_ALERT "copy from user error(2), abort this write.\n");
            spin_unlock(&transfer_lock);
            return 0;
        }
        msg_Ptr = msg + (len - len_to_end) - 1;
        // make sure it ends at word boundary
        while ((int) msg_Ptr % 4)
            *(++msg_Ptr) = 0;

        // for ( ; (int) msg_Ptr % 4; msg_Ptr++)
        //     *msg_Ptr = 0;
        magic_mem[10] = __pa(msg_Ptr);
    } else 
    {
        ret = copy_from_user(msg_Ptr + 1, buff, len);
        if (ret > 0)
        {
            printk(KERN_ALERT "copy from user error(3) failing, abort this write.\n");
            spin_unlock(&transfer_lock);
            return 0;
        }
        // msg_Ptr = &(msg_Ptr[len]);
        msg_Ptr = msg_Ptr + len;
        // make sure it ends at word boundary
        while ((int) msg_Ptr % 4)
            *(++msg_Ptr) = 0;
        magic_mem[10] = __pa(msg_Ptr);
    }

    spin_unlock(&transfer_lock);

    return len;
}
