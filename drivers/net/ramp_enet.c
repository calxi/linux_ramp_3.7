#include <linux/module.h>
#include <asm/ramp.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/mm_types.h>
#include <linux/skbuff.h>
#include <linux/ethtool.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/ip.h>

#include "..//arch/sparc/kernel/irq.h"

#define DRIVER_NAME "RAMP Eth MAC driver"
#define DRIVER_VERSION "1.0"

MODULE_AUTHOR("Zhangxi Tan <xtan@cs.berkeley.edu>");
MODULE_DESCRIPTION(DRIVER_NAME);
MODULE_LICENSE("BSD");

#define MAX_DEVS 1      //maximum NIC supported 

#define DESC_SIZE   32  //descriptor size
#define MAX_RX_DESC 256
#define MAX_TX_DESC 128

#define RBSIZE   5      //receive buffer size 8192 bytes
#define RD_LEN   2      
#define TD_LEN   1
#define FLITSIZE 64

#define RAMP_ENET_MAX_MTU       8160    //recv buffer - 32 for RX alignment     
#define RAMP_ENET_DEFAULT_MTU   1500

#define RX_SKB_ALIGN 64                 //must be flit aligned

#define RAMP_NAPI_WEIGHT        64
#define TX_INTR_THRESHOLD       16      //renable the tx interrupt when we don't have enough descriptors

typedef struct __attribute__ ((aligned (1))){
    uint64_t        addr;
    uint16_t        len;
    char            eop;
    char            pad[21];
}nic_descriptor_t;

struct ramp_enet_private {
    //ring buffer data structures
    nic_descriptor_t  *rx_desc_raw, *tx_desc_raw;   //raw pointer (for kfree) before alignment
    nic_descriptor_t  *rx_desc, *tx_desc;

    struct {
        struct sk_buff *skb;
        uint8_t eop;            //end of flit
    }tx_ring[MAX_TX_DESC];          //tx ring info used to free sent skbs

    struct {
        struct sk_buff *skb;
        //                uint8_t flag;           //used for debugging
    }rx_ring[MAX_RX_DESC]; 

    struct {
        uint16_t head, tail;    //will read from head from nic to decide when to free TX buffer
        uint16_t nfree_desc;
    }tx;

    struct {
        uint16_t   tail;     //rx tail from HW might be stale (updated every flit), so use the point maintained by the driver 
    }rx;

    uint32_t           tx_irq_mask;
    spinlock_t         nfree_lock, cleanup_lock;  
    uint8_t            tx_cleanup_busy;
    uint16_t           mtu_a;       //aligned MTU to flit size
    struct napi_struct napi;

    //pointer to this and other ramp_enet device
    struct net_device *prev_dev, *dev;
};

struct net_device *dev_list; //point to the last RAMP enet device

static int virt_irqs[2];

static void flush_dcache() 
{
    unsigned long faddr = 0;   

    //flush 8 lines 
    __asm__ __volatile__("sta %%g0, [%1] %0\n\t"
            "sta %%g0, [%1 + %2] %0\n\t"
            "sta %%g0, [%1 + %3] %0\n\t"
            "sta %%g0, [%1 + %4] %0\n\t"
            "sta %%g0, [%1 + %5] %0\n\t"
            "sta %%g0, [%1 + %6] %0\n\t"
            "sta %%g0, [%1 + %7] %0\n\t"
            "sta %%g0, [%1 + %8] %0\n\t" : :                              
            "i"(ASI_M_FLUSH_BYPASS), "r" (faddr), "r" (0x20), "r" (0x40), 
            "r" (0x60), "r" (0x80), "r" (0xa0), "r"(0xc0), "r"(0xe0)
            : "memory");       
}

static int ramp_enet_close(struct net_device *dev)
{
    struct ramp_enet_private *tp = netdev_priv(dev); 
    int i;

    netif_stop_queue(dev);  
    napi_disable(&tp->napi);

    ramp_store_reg(dev->base_addr | 8, 0 | (RBSIZE << 8) | (TD_LEN << 16) | (RD_LEN << 24) | ((FLITSIZE == 64)<< 31));  //stop the world first but I don't want to mess up with the FSM

    free_irq(virt_irqs[0], dev);
    free_irq(virt_irqs[1], dev);

    //free any skb on the ring buffer
    for (i=0;i<MAX_RX_DESC;i++) {
        if (tp->rx_ring[i].skb)
            dev_kfree_skb(tp->rx_ring[i].skb);
    }

    for (i=0;i<MAX_TX_DESC;i++) {
        if (tp->tx_ring[i].eop && tp->tx_ring[i].skb)
            dev_kfree_skb(tp->tx_ring[i].skb);              
    }

    //free allocated ring buffers here
    kfree(tp->rx_desc_raw);
    kfree(tp->tx_desc_raw);
    return 0;
}

static inline void *ramp_enet_align(void *data)         //ramp gold cache line aligned
{
    return (void *)ALIGN((long)data, 32);
}

static inline void ramp_enet_inc_queue_pt(uint16_t *pt, const uint16_t max_pt)
{
    *pt = (*pt + 1) % max_pt;
}

//rx routine
static void ramp_enet_rx(struct net_device *dev)  
{
    static uint32_t  last_head, last_tail, rx_irq_cnt=0, last_write_tail;

    struct sk_buff *skb;
    //      uint32_t        flag = ramp_load_reg(dev->base_addr | 8) & 1;
    uint32_t        data = ramp_load_reg(dev->base_addr);
    uint32_t        head;
    uint16_t        tail;

    head = data >> 16;      
    struct ramp_enet_private *tp = netdev_priv(dev); 

    rx_irq_cnt++;
    tail = data & 0xffff;

    if (tail == head) {
        uint32_t flag = ramp_load_reg(dev->base_addr | 8) & 1;
        if (flag) {                                     
            pr_emerg("%s: rx saw an redundant interrupt, head=%d, last_head=%d, last_tail=%d, rx_cnt=%d, last_write_tail=%d\n", dev->name, head, last_head, last_tail,rx_irq_cnt, last_write_tail);  
            goto rx_exit;
        }
    }

    last_head = head;
    last_tail = tail;       

    //printk("rx_irq_cnt=%d, head=%d, tail=%d\n", rx_irq_cnt, head, tail); 
    do {
        skb = dev_alloc_skb(RAMP_ENET_MAX_MTU + RX_SKB_ALIGN + 32);

        if (likely(skb)) {
            int i; 
            uint32_t flush_base_addr;
            struct sk_buff *oldskb = tp->rx_ring[tail].skb;
            //recover the first 64-bit of the packet (our HW doesn't write the first 64-bit)
            /*for (i=0;i<6;i++)
              oldskb->data[i] = dev->dev_addr[i];*/

            //printk("process tail %d\n", tail);

            unsigned int len = FLITSIZE * __le16_to_cpu((*(uint16_t*)(&oldskb->data[0])));                  
            unsigned long p, id;            

            dev->stats.rx_packets++;                        
            dev->stats.rx_bytes += len;

            //printk("desc addr %x skb_data addr %x, tail=%d, head=%d\n",__pa(&tp->rx_desc[tail]),  __pa(oldskb->data), tail, head);
            //                      printk("rx see a packet len = %d, from %x:%x:%x:%x \n", len, oldskb->data[8], oldskb->data[9], oldskb->data[10], oldskb->data[11]);             
            //printk("rx_desc[%d].addr %lx\n", tail, tp->rx_desc[tail].addr);
            //                      printk("before sum irq_cnt=%d\n", rx_irq_cnt);

            /*                      uint8_t sum =0, oldsum, sumlen;
                                    oldsum = oldskb->data[6]; sumlen = oldskb->data[7];
             *(uint16_t*)(&oldskb->data[6]) = 0x7c7c; //patch back
             if (len == 128) {
             int i;
             for (i=0;i<sumlen;i++)
             sum+=oldskb->data[i];
             if (oldsum != sum)
             printk("rx chksum_error from  %x:%x:%x:%x, rx_irq_cnt=%d, sum=%d, expect=%d, len=%d\n", oldskb->data[8], oldskb->data[9], oldskb->data[10], oldskb->data[11], rx_irq_cnt, sum, oldsum, sumlen);
             }*/

            /*uint8_t *q = oldskb->data;
              int j;
              uint32_t tmp;
              for (i=0;i<32;i++) {
              tmp = 0;
              for (j=0;j<4;j++) {
              tmp = (tmp << 8) | *q;
              q++;
              }
              printk("%08x\n", tmp);
              }*/

            if (len == 0 ||  len > 1536 ) { //(len != 1536 && len != 128 && len !=192))
                printk("Illegal RX length %d, rx_irq_cnt=%d, payload addr=%x\n", len, rx_irq_cnt, __pa(oldskb->data));

            }

            //asm volatile("mov %%asr15,%0" : "=&r"(id) : );
            if (oldskb->data[3] !=dev->dev_addr[3] || oldskb->data[4] != dev->dev_addr[4] || oldskb->data[5] != dev->dev_addr[5] || oldskb->data[2] != dev->dev_addr[2])
                printk("Illegal dest to %x:%x:%x:%x \n", oldskb->data[2], oldskb->data[3], oldskb->data[4], oldskb->data[5]);


            *(uint16_t*)(&oldskb->data[0]) = 0x7c7c; //patch back

            /*printk("packet dump:\n");
              for ( i=0;i<len; i++)
              printk("%x ",oldskb->data[i]);
              printk("\n");*/

            skb_put(oldskb, len);   //fix the length field  
            oldskb->protocol = eth_type_trans(oldskb, dev);         //generate the protocol field from the packet

            oldskb->ip_summed = CHECKSUM_UNNECESSARY;               //pretend we have a CSUM offloading engine
            netif_rx(oldskb);       
            //                      printk("rx_irq_cnt = %d, tail=%d\n", rx_irq_cnt, tail);
            p = (unsigned long) skb->data;
            skb_reserve(skb, ((p + RX_SKB_ALIGN - 1) & ~(RX_SKB_ALIGN - 1)) - p + 2);
            tp->rx_ring[tail].skb = skb;

            tp->rx_desc[tail].addr = __cpu_to_le64(__pa(skb->data));

            //flush the descriptor  
            asm __volatile__("sta %%g0, [%1] %0"
                    ::"i"(ASI_M_DFLUSH), "r"(&tp->rx_desc[tail]) :"memory");

            //flush the data cache as NIC DMA is not coherent
            flush_base_addr = ((uint32_t)skb->data) & ~0x1f;
            for (i=0;i<32*8;i+=32) {
                asm __volatile__("sta %%g0, [%2+%1] %0"
                        ::"i"(ASI_M_DFLUSH), "r"(flush_base_addr), "r"(i) :"memory");          
            }
        } 
        else    {
rx_drop:
            dev->stats.rx_dropped++;
        }

        ramp_enet_inc_queue_pt(&tail, MAX_RX_DESC);
    }while (tail != head);

    asm __volatile__("stbar" :::"memory");
    ramp_store_reg(dev->base_addr, tail);   
    last_write_tail = tail;
rx_exit:
    ramp_store_reg(dev->base_addr | 20, 1 | tp->tx_irq_mask);  //re enable the rx irq

}

static void inline spin_lock_any(spinlock_t *lock) {
    if (in_irq()) {
        spin_lock(lock);
    } else {
        spin_lock_irq(lock);     
    }
}

static void inline spin_unlock_any(spinlock_t *lock) {
    if (in_irq()) {
        spin_unlock(lock);
    } else {
        spin_unlock_irq(lock);     
    }
}

//This cleanup handler might be called before the kernel respond the tx interrupt
static inline void ramp_enet_tx_cleanup(struct net_device *dev, struct ramp_enet_private *tp)
{
    // unsigned long _flags;
    // raw_local_save_flags(_flags);
    spin_lock_any(&tp->cleanup_lock);
    // raw_local_save_flags(_flags);
    if (tp->tx_cleanup_busy) {                //someone else is doing the cleanup, just be patient
        spin_unlock_any(&tp->cleanup_lock);
        return;
    }        
    tp->tx_cleanup_busy = 1;    
    // raw_local_save_flags(_flags);
    spin_unlock_any(&tp->cleanup_lock);        //we don't want to hold up other interrupts for long
    // raw_local_save_flags(_flags);

    {       
        uint32_t tmp, head;
        uint16_t free_cnt = 0;

        tmp = ramp_load_reg(dev->base_addr | 4);  //read tx head pointer from nic
        head = tmp >> 16;

        //        last_tx_head = head;            //debugging;
        //        last_nfree = tp->tx.nfree_desc;

        if (tp-> tx.nfree_desc ==0 && (tmp & 1) && (head == tp->tx.head))      //this is an extremely rare case, where we have to free all TX descriptors
            goto force;

        while (head != tp->tx.head) {
force:          if (tp->tx_ring[tp->tx.head].eop) {
                    tp->tx_ring[tp->tx.head].eop = 0;
                    // raw_local_save_flags(_flags);
                    dev_kfree_skb_any(tp->tx_ring[tp->tx.head].skb);
                    // raw_local_save_flags(_flags);
                }
                ramp_enet_inc_queue_pt(&tp->tx.head, MAX_TX_DESC);
                free_cnt++;     
        }
        //       last_free = free_cnt;        

        if (free_cnt) {
            spin_lock_any(&tp->nfree_lock);
            tp->tx.nfree_desc += free_cnt;
            spin_unlock_any(&tp->nfree_lock);

            if (netif_queue_stopped(dev)) {
                netif_wake_queue(dev);          //wake up the tx queue as we have free descriptors
                printk("%s: tx queue restarted\n", dev->name);
            }
        }
        else if (netif_queue_stopped(dev)) {                            
            tp->tx_irq_mask = 2;
            ramp_store_reg(dev->base_addr | 20, tp->tx_irq_mask);    //make sure the tx irq is online to wake this queue up
        }                
    }
    spin_lock_any(&tp->cleanup_lock);           //acquire the lock before we change it back
    tp->tx_cleanup_busy = 0;
    spin_unlock_any(&tp->cleanup_lock);
}

//static uint16_t last_rx_tail, last_rx_head;

static int ramp_enet_poll(struct napi_struct *napi, int budget)
{
    struct sk_buff *skb;
    uint32_t        head;
    uint16_t        tail;
    int             poll_cnt;

    struct ramp_enet_private *tp = container_of(napi, struct ramp_enet_private, napi);
    struct net_device *dev = tp->dev;

    uint32_t        data = ramp_load_reg(dev->base_addr);
    uint32_t        flag = ramp_load_reg(dev->base_addr | 8) & 1;

    //reclaim tx buffer
    ramp_enet_tx_cleanup(dev, tp);

    head = data >> 16;      
    poll_cnt = 0;
    tail = data & 0xffff;

    //	last_rx_head = head;
    if ((tail == head) && (flag >0)) //HW is telling us the rx queue is empty
        goto rx_done;

    //	if (dev->dev_addr[2]==0)  {
    //		printk("head=%d tail=%d rx.tail=%d\n", head, tail, tp->rx.tail);
    //	}
    tail = tp->rx.tail;     //use the most up-to-date tail pointer from the driver


    while ((poll_cnt < budget)  && (tail != head))
    {
        //printk("head=%d, tail=%d, flag=%d\n", head, tail, flag);
        uint32_t new_data;

        skb = dev_alloc_skb(RAMP_ENET_MAX_MTU + RX_SKB_ALIGN + 32);

        if (likely(skb)) {
            int i; 
            struct sk_buff *oldskb = tp->rx_ring[tail].skb;

            unsigned int len = FLITSIZE * __le16_to_cpu((*(uint16_t*)(&oldskb->data[0])));                  
            unsigned long p, id;            

            dev->stats.rx_packets++;                        
            dev->stats.rx_bytes += len;

            //printk("desc addr %x skb_data addr %x, tail=%d, head=%d\n",__pa(&tp->rx_desc[tail]),  __pa(oldskb->data), tail, head);
            //printk("rx see a packet len = %d, from %x:%x:%x:%x \n", len, oldskb->data[8], oldskb->data[9], oldskb->data[10], oldskb->data[11]);           
            //printk("rx_desc[%d].addr %lx\n", tail, tp->rx_desc[tail].addr);


            /*uint8_t *q = oldskb->data;
              int j;
              uint32_t tmp;
              for (i=0;i<32;i++) {
              tmp = 0;
              for (j=0;j<4;j++) {
              tmp = (tmp << 8) | *q;
              q++;
              }
              printk("%08x\n", tmp);
              }*/

            if (len == 0 ||  len > tp->mtu_a ) { //(len != 1536 && len != 128 && len !=192))
                //printk("Illegal RX length %x, payload addr=%x, tail=%d, head=%d, flag=%d, budget=%d, hw_new_tail=%d, hw_old_head=%d, hw_old_tail=%d, poll_cnt=%d, last_rx_tail=%d, last_rx_head = %d\n", len/FLITSIZE,  __pa(oldskb->data), tail, head, flag, budget, new_data & 0xffff, data >> 16, data & 0xffff, poll_cnt, last_rx_tail, last_rx_head);
                printk("Illegal RX length %x, payload addr=%x, tail(driver)=%d, head=%d, flag=%d, budget=%d, hw_new_tail=%d, hw_old_head=%d, hw_old_tail=%d, poll_cnt=%d\n", len/FLITSIZE,  __pa(oldskb->data), tail, head, flag, budget, new_data & 0xffff, data >> 16, data & 0xffff, poll_cnt);

                dev_kfree_skb(skb);
                goto rx_poll_drop;
            }

            //asm volatile("mov %%asr15,%0" : "=&r"(id) : );
            if (oldskb->data[3] !=dev->dev_addr[3] || oldskb->data[4] != dev->dev_addr[4] || oldskb->data[5] != dev->dev_addr[5] || oldskb->data[2] != dev->dev_addr[2]) 
                printk("Illegal dest to %x:%x:%x:%x \n", oldskb->data[2], oldskb->data[3], oldskb->data[4], oldskb->data[5]);


            *(uint16_t*)(&oldskb->data[0]) = 0x7c7c; //patch back


            skb_put(oldskb, len);   //fix the length field  
            oldskb->protocol = eth_type_trans(oldskb, dev);         //generate the protocol field from the packet

            oldskb->ip_summed = CHECKSUM_UNNECESSARY;               //pretend we have a CSUM offloading engine
            netif_receive_skb(oldskb);      
            poll_cnt++;

            //                      printk("rx_irq_cnt = %d, tail=%d\n", rx_irq_cnt, tail);
            p = (unsigned long) skb->data;
            skb_reserve(skb, ((p + RX_SKB_ALIGN - 1) & ~(RX_SKB_ALIGN - 1)) - p + 2);
            tp->rx_ring[tail].skb = skb;

            tp->rx_desc[tail].addr = __cpu_to_le64(__pa(skb->data));

            //flush the rx descriptor                       
            //asm __volatile__("sta %%g0, [%1] %0"
            //          ::"i"(ASI_M_DFLUSH), "r"(&tp->rx_desc[tail]) :"memory");

        } 
        else    {

rx_poll_drop:       
            dev->stats.rx_dropped++;
        }

        ramp_enet_inc_queue_pt(&tail, MAX_RX_DESC);

        //flush the decache before we poll for rx data again.
        flush_dcache();
        asm __volatile__("stbar" :::"memory");
        ramp_store_reg(dev->base_addr, tail);            //free the descriptor so we can use it
        tp->rx.tail = tail;

        //read head pointer again (head might advance while polling)
        //to make everything simple ignore flag and let intr help us. We just increment tail, but have yet committed our change. Head != tail will be always true
        //flag = ramp_load_reg(dev->base_addr | 8) & 1; 
        //head = ramp_load_reg(dev->base_addr) >> 16;
        new_data = ramp_load_reg(dev->base_addr);
        head = new_data >> 16;
        //		last_rx_head = head;
    }
    //      printk("new tail = %d, head = %d, poll_cnt=%d, budget=%d\n", tail, head, poll_cnt, budget);

rx_done:
    if (poll_cnt < budget) {
        napi_complete(napi);                     //napi poll is done
        ramp_store_reg(dev->base_addr | 20, 1 | tp->tx_irq_mask);  //re-enable the rx interrupt only
    }

    return poll_cnt;
} 


//rx interrupt
static irqreturn_t ramp_enet_rx_interrupt(int irq, void *dev_instance)
{
    // printk("ramp_enet_rx_interrupt begins\n");
    struct net_device *dev = dev_instance;
    struct ramp_enet_private *tp = netdev_priv(dev); 

#if 0               
    ramp_enet_rx(dev);
#endif
    //napi rx interrupt handler

    //turn off the rx interrupt and schedule netpoll
    //rx interrupt is automatically turned off by the HW
    if (likely(napi_schedule_prep(&tp->napi))) 
        __napi_schedule(&tp->napi);

    return IRQ_RETVAL(1);
}

//tx interrupt
static irqreturn_t ramp_enet_tx_interrupt(int irq, void *dev_instance)
{
    // printk("begins ramp_enet_tx_interrupt\n");
    unsigned long _flags;
    raw_local_save_flags(_flags);
    // printk("irq flags: %lx, is disabled: %i\n", _flags, (irqs_disabled() ? 1 : 0));
    struct net_device *dev = dev_instance;
    struct ramp_enet_private *tp = netdev_priv(dev); 

    // disable_irq(irq);
    //if (netif_queue_stopped(dev)) 
    //      netif_wake_queue(dev);          //wake up the tx queue as we have free descriptors

    // disable tx IRQ
    tp->tx_irq_mask = 0;
    ramp_store_reg(dev->base_addr | 20, 0);    


    //if (likely(napi_schedule_prep(&tp->napi)))  //turn on polling to perform clean up 
    //      __napi_schedule(&tp->napi);
    ramp_enet_tx_cleanup(dev, tp);             //call clean up 

    // printk("ends ramp_enet_tx_interrupt\n");

    raw_local_save_flags(_flags);
    // printk("irq flags: %lx, is disabled: %i\n", _flags, (irqs_disabled() ? 1 : 0));
    return IRQ_RETVAL(1);
}
static int ramp_enet_open(struct net_device *dev)
{
    struct ramp_enet_private *tp = netdev_priv(dev); 
    int ret = -ENOMEM;
    int i;
    unsigned int tail;

    //allocate nic descriptors
    tp->rx_desc_raw = kmalloc(MAX_RX_DESC * DESC_SIZE, GFP_KERNEL); 
    if (tp->rx_desc_raw == 0)
        goto err_out;

    if (ramp_enet_align(tp->rx_desc_raw) != (void *)(tp->rx_desc_raw)) {
        kfree(tp->rx_desc_raw);
        tp->rx_desc_raw = kmalloc(MAX_RX_DESC * DESC_SIZE+31, GFP_KERNEL);
        if (tp->rx_desc_raw == 0)
            goto err_out;
    }
    tp->rx_desc = (nic_descriptor_t*)ramp_enet_align(tp->rx_desc_raw);

    for (i = 0;i < MAX_RX_DESC; i++)
        tp->rx_ring[i].skb = NULL;
    for (i = 0;i < MAX_TX_DESC; i++) {
        tp->tx_ring[i].skb = NULL;
        tp->tx_ring[i].eop = 0;
    }

    //allocate RX buffers
    for (i = 0;i < MAX_RX_DESC; i++) {
        struct sk_buff *skb;

        skb = __netdev_alloc_skb(dev, RAMP_ENET_MAX_MTU + RX_SKB_ALIGN + 32, GFP_KERNEL);
        tp->rx_ring[i].skb = skb;
        if (likely(skb)) {
            unsigned long p = (unsigned long) skb->data;
            skb_reserve(skb, ((p + RX_SKB_ALIGN - 1) & ~(RX_SKB_ALIGN - 1)) - p + 2);
            tp->rx_desc[i].addr = __cpu_to_le64(__pa(skb->data));

            //TODO remove this
            //tp->rx_desc[i].len = 0xdead;
            //if (i<16) 
            //      printk("RX[%d]=%x\n", i,__pa(skb->data));


            asm __volatile__("sta %%g0, [%1] %0"
                    ::"i"(ASI_M_DFLUSH), "r"(&tp->rx_desc[i]) :"memory");
        } 
        else
            break;
    }

    if (i!= MAX_RX_DESC) {
        int j;
        pr_emerg("%s: no memory for rx ring\n", dev->name);
        for(j=0;j<i;j++) {
            if (tp->rx_ring[j].skb) {
                dev_kfree_skb(tp->rx_ring[j].skb);
                tp->rx_ring[j].skb = NULL;
            }
        }
        goto err_out;
    }


    tp->tx_desc_raw = kmalloc(MAX_TX_DESC * DESC_SIZE, GFP_KERNEL); 
    if (tp->tx_desc_raw == 0) 
        goto err_out;                           

    if (ramp_enet_align(tp->tx_desc_raw) != (void *)(tp->tx_desc_raw)) {
        kfree(tp->tx_desc_raw);
        tp->tx_desc_raw = kmalloc(MAX_TX_DESC * DESC_SIZE+31, GFP_KERNEL);
        if (tp->tx_desc_raw == 0) 
            goto err_out;
    }
    tp->tx_desc = (nic_descriptor_t*)ramp_enet_align(tp->tx_desc_raw);

    //initialize ring buffer tx pointers
    tail = ramp_load_reg(dev->base_addr | 4) >> 16;
    tp->tx.head = tail; tp->tx.tail = tail;
    tp->tx.nfree_desc = MAX_TX_DESC; 

    //program nic registers
    //program the rx tail pointer
    tail = ramp_load_reg(dev->base_addr) >> 16;
    tp->rx.tail = tail;
    ramp_store_reg(dev->base_addr, tail);           //start with head = tail
    //program the rx/tx descriptor base address
    ramp_store_reg(dev->base_addr | 12, __pa((unsigned int)tp->rx_desc) >> 5);
    ramp_store_reg(dev->base_addr | 16, __pa((unsigned int)tp->tx_desc) >> 5);

    napi_enable(&tp->napi); 
    //program command register
    asm __volatile__("stbar":::"memory");

    ramp_store_reg(dev->base_addr | 8, 3 | (RBSIZE << 8) | (TD_LEN << 16) | (RD_LEN << 24) | ((FLITSIZE == 64)<< 31));

    //request IRQs (irq can be shared in the future)
    ret = request_irq(virt_irqs[0], ramp_enet_rx_interrupt, 0, dev->name, dev);
    if (ret <0)
        goto err_out;
    ret = request_irq(virt_irqs[1], ramp_enet_tx_interrupt, 0, dev->name, dev);
    if (ret < 0) {
        free_irq(virt_irqs[0], dev);        //free rx irq
        goto err_out; 
    }

    return 0;
err_out:
    kfree(tp->rx_desc_raw);
    kfree(tp->tx_desc_raw);
    return ret;
}

static netdev_tx_t ramp_enet_start_xmit(struct sk_buff *skb,
        struct net_device *dev)

{
    struct ramp_enet_private *tp = netdev_priv(dev); 
    int tx_err;
    static int tx_err_cnt=0;
    // printk("original dst mac %x:%x:%x:%x:%x:%x, len=%d, nr_frag=%d\n", skb->data[0], skb->data[1], skb->data[2], skb->data[3], skb->data[4], skb->data[5], skb->len, skb_shinfo(skb)->nr_frags);
    //TX ring is full, first try to free some space (lazy free)

    //source routing       
    struct iphdr* t_iph;
    t_iph = ip_hdr(skb);
    // printk("ip dest address : %x\n",t_iph->daddr);        
    // printk("ip source address : %x\n",t_iph->saddr);      

    skb->data[2] = (t_iph->daddr-1) & 0xff;
    skb->data[3] = (t_iph->daddr >> 8) & 0xff;
    skb->data[4] = (t_iph->daddr >> 16) & 0xff;
    //      skb->data[5] = (t_iph->daddr >> 24) & 0xff;
    skb->data[5] = 0;

    //printk("dst mac after source routing %x:%x:%x:%x:%x:%x, len=%d, nr_frag=%d\n", skb->data[0], skb->data[1], skb->data[2], skb->data[3], skb->data[4], skb->data[5], skb->len, skb_shinfo(skb)->nr_frags);
    //printk("src mac %x:%x:%x:%x:%x:%x\n", skb->data[6], skb->data[7], skb->data[8], skb->data[9], skb->data[10], skb->data[11]);

    //check available tx descriptor
    spin_lock_irq(&tp->nfree_lock);                 //tx/rx interrupt may change this value
    if (tp->tx.nfree_desc < TX_INTR_THRESHOLD) {      //re-enable the tx interrupt before all descriptors are running out
        tp->tx_irq_mask = 2;
        ramp_store_reg(dev->base_addr | 20, 2);
    }

    if (tp->tx.nfree_desc < skb_shinfo(skb)->nr_frags + 1)  
        tx_err = 1;     
    else {
        tx_err = 0;

        tp->tx.nfree_desc -= skb_shinfo(skb)->nr_frags+1;       
    }       
    spin_unlock_irq(&tp->nfree_lock);  

    if (tx_err) {
        //try to clean the tx queue up before we stop the world
        ramp_enet_tx_cleanup(dev, tp);             //do an emergence clean up 

        if (tp->tx.nfree_desc < skb_shinfo(skb)->nr_frags + 1) { 
            //TX ring is full
            spin_lock_irq(&tp->cleanup_lock);     //can't let rx irq/preemption happens 
            if (!tp->tx_cleanup_busy) {   //no one is doing the dirty work
                //                                pr_emerg("%s (%d): TX ring is full when queue awake! free=%d, request=%d, head=%d, last_tx_head=%d, tx_irq_en=%d, last_free_cnt=%d, nfree=%d\n", dev->name, tx_err_cnt++,  tp->tx.nfree_desc, skb_shinfo(skb)->nr_frags + 1, tp->tx.head, last_tx_head, tp->tx_irq_mask, last_free, last_nfree);
                pr_emerg("%s (%d): TX ring is full when queue awake! free=%d, request=%d, head=%d, tx_irq_en=%d\n", dev->name, tx_err_cnt++,  tp->tx.nfree_desc, skb_shinfo(skb)->nr_frags + 1, tp->tx.head, tp->tx_irq_mask);
                netif_stop_queue(dev);
                spin_unlock_irq(&tp->cleanup_lock);
                tp->tx_irq_mask = 2;
                ramp_store_reg(dev->base_addr | 20, 2);  //make sure we re-enable the tx interrupt
            } 
            else
                spin_unlock_irq(&tp->cleanup_lock);                                

            goto err_stop;
        }

        spin_lock_irq(&tp->nfree_lock);              
        tp->tx.nfree_desc -= skb_shinfo(skb)->nr_frags+1;                               
        spin_unlock_irq(&tp->nfree_lock);   
    }
    //patch the first two bytes with # of flits in the mac header
    uint16_t tx_len; 
    tx_len = (uint16_t)((skb->len + FLITSIZE-1) / FLITSIZE);
    skb->data[0] = tx_len & 0xff; 
    skb->data[1] = tx_len >> 8;


    //calculate chksum
    /*uint8_t sum=0;

      if (skb->len < 128) {
      if (skb_shinfo(skb)->nr_frags)
      printk("multiple segment len=%d, first=%d\n", skb->len, skb_headlen(skb));

      int i;
      for (i=0;i<skb->len;i++)
      sum+=skb->data[i];
      skb->data[6] = sum;
      skb->data[7] = skb->len;
      }*/


    if (!skb_shinfo(skb)->nr_frags) {       //no paged data (common case)?          
        // printk("skb address %x\n", (unsigned int) skb);
        tp->tx_ring[tp->tx.tail].skb = skb;
        tp->tx_ring[tp->tx.tail].eop = 1;

        tp->tx_desc[tp->tx.tail].addr = __cpu_to_le64(__pa(skb->data));
        tp->tx_desc[tp->tx.tail].len = __cpu_to_le16(skb->len);  //ok to truncate this this value to 16-bit, as our MTU << 64K
        tp->tx_desc[tp->tx.tail].eop = 1;

        //      printk("xmit: tx.tail = %d, addr = %x, len = %d\n", tp->tx.tail, __pa(skb->data), skb->len);

        //              asm __volatile__("sta %%g0, [%1] %0"
        //                               ::"i"(ASI_M_DFLUSH), "r"(&tp->tx_desc[tp->tx.tail]) :"memory");

        /*      uint8_t *p = skb->data;
                uint32_t tmp;
                int i,j;
                for (i=0;i<32;i++) {
                tmp = 0;
                for (j=0;j<4;j++) {
                tmp = (tmp << 8) | *p;
                p++;
                }
                printk("%08x\n", tmp);
                }*/

        ramp_enet_inc_queue_pt(&tp->tx.tail, MAX_TX_DESC);
    }
    else {
        int i; 
        //write the non-paged data
        tp->tx_ring[tp->tx.tail].eop = 0;

        tp->tx_desc[tp->tx.tail].addr = __cpu_to_le64(__pa(skb->data));
        tp->tx_desc[tp->tx.tail].len = __cpu_to_le16(skb_headlen(skb));  //ok to truncate this this value to 16-bit, as our MTU << 64K
        tp->tx_desc[tp->tx.tail].eop = 0;

        //              asm __volatile__("sta %%g0, [%1] %0"
        //                               ::"i"(ASI_M_DFLUSH), "r"(&tp->tx_desc[tp->tx.tail]) :"memory");
        ramp_enet_inc_queue_pt(&tp->tx.tail, MAX_TX_DESC);

        for (i=0; i < skb_shinfo(skb)->nr_frags;i++) {
            skb_frag_t *frag = &skb_shinfo(skb)->frags[i];

            // HACK! 
            tp->tx_desc[tp->tx.tail].addr = __cpu_to_le64(__pa((unsigned int)page_address(frag->page.p) + frag->page_offset));

            tp->tx_desc[tp->tx.tail].len = __cpu_to_le16(frag->size);
            if (i == skb_shinfo(skb)->nr_frags-1) {
                tp->tx_ring[tp->tx.tail].eop =  1;
                tp->tx_ring[tp->tx.tail].skb = skb;
                tp->tx_desc[tp->tx.tail].eop = 1;
            }
            else {
                tp->tx_ring[tp->tx.tail].eop = 0;
                tp->tx_desc[tp->tx.tail].eop = 0;       
            }

            //                      asm __volatile__("sta %%g0, [%1] %0"
            //                               ::"i"(ASI_M_DFLUSH), "r"(&tp->tx_desc[tp->tx.tail]) :"memory");
            ramp_enet_inc_queue_pt(&tp->tx.tail, MAX_TX_DESC);                      
        }
    }

    //ensure the NIC see the descriptor in memory
    flush_dcache();
    //printk("xmit: after flush\n");
    asm __volatile__( "stbar":::"memory");
    //printk("xmit: after stbar\n");
    ramp_store_reg(dev->base_addr | 4, (uint32_t)(tp->tx.tail)); //update the tail pointer to start transfer
    //      printk("sum=%d\n",sum);

    dev->stats.tx_packets++;
    dev->stats.tx_bytes += skb->len;

    // printk("xmit %ld packets\n", dev->stats.tx_packets);
    return NETDEV_TX_OK;
err_stop:
    // printk("xmit error return\n");
    dev->stats.tx_dropped++;
    return NETDEV_TX_BUSY;
}

static int ramp_enet_change_mtu(struct net_device *dev, int new_mtu)
{
    struct ramp_enet_private *tp = netdev_priv(dev); 

    if (new_mtu < ETH_ZLEN || new_mtu > RAMP_ENET_MAX_MTU)
        return -EINVAL;

    dev->mtu = new_mtu;
    tp->mtu_a = (new_mtu + RX_SKB_ALIGN - 1) & ~(RX_SKB_ALIGN - 1);

    return 0;
}

static struct net_device_stats *ramp_enet_get_stats(struct net_device *dev)
{
    return &dev->stats;
}

static int ramp_enet_ioctl(struct net_device *dev, struct ifreq *ifr, int cmd)
{
    //      printk("%s, ioctl called\n", dev->name);
    return -EOPNOTSUPP;     //we don't have the MII stuff
}

static int ramp_enet_set_mac_address(struct net_device *dev, void *p)   //usually user shouldn't change the MAC addr
{
    struct sockaddr *addr = p;

    //      printk("%s, set_mac_address is called\n", dev->name);
    if (!is_valid_ether_addr(addr->sa_data))
        return -EADDRNOTAVAIL;

    memcpy(dev->dev_addr, addr->sa_data, dev->addr_len);

    return 0;
}

//we can't use the standard eth_validate_addr, as we re-encode the ethernet mac address
static int ramp_enet_validate_addr(struct net_device *dev)
{
    if (dev->dev_addr[0] != 0x7c || dev->dev_addr[1] != 0x7c)  //LSB of the first byte must be 0 (unicast), otherwise it will affect TCP, by setting skb->pkt_type to multicast.
        return -EADDRNOTAVAIL;

    return 0;
}

static const struct net_device_ops ramp_netdev_ops = {
    .ndo_open               = ramp_enet_open,
    .ndo_stop               = ramp_enet_close,
    .ndo_get_stats          = ramp_enet_get_stats,
    .ndo_change_mtu         = ramp_enet_change_mtu,
    .ndo_validate_addr      = ramp_enet_validate_addr,
    .ndo_set_mac_address    = ramp_enet_set_mac_address,
    .ndo_start_xmit         = ramp_enet_start_xmit,
    .ndo_do_ioctl           = ramp_enet_ioctl
};



static void __devinit ramp_enet_init_devs (struct net_device *dev, unsigned long dev_id)
{
    unsigned long id;

    //static initializations
    dev->netdev_ops = &ramp_netdev_ops;
    //no checksum and no arps for now
    // HACK! <- i think it's ok as long as we leave csum bit off
    dev->features |= NETIF_F_SG; // | NETIF_F_NO_CSUM;
    dev->flags |= IFF_NOARP;
    dev->flags &= ~(IFF_BROADCAST | IFF_MULTICAST); //turn off broadcast and multicast packet on our interface for now

    //Device address to dev_addr (board id/FPGA id/pipe_id/TID)
    dev->addr_len = 6;
    asm volatile("mov %%asr15,%0" : "=&r"(id) : );
#ifdef CONFIG_SINGLE_RACK
    id &= 0xff;
#endif
    printk("RAMP enet core id %x\n", id);
    dev->dev_addr[2] = (id & 0xff) | (dev_id << 6);
    dev->dev_addr[3] = (id >> 8) & 0xff;
    dev->dev_addr[4] = (id >> 16) & 0xff;
    dev->dev_addr[5] = (id >> 24) & 0xff;
    dev->dev_addr[0] = 0x7c;   //this two field will be patched
    dev->dev_addr[1] = 0x7c;

    //HW related settings
    dev->irq = RAMP_ENET_INTNO + dev_id*2;
    virt_irqs[0] = sparc_config.build_device_irq(NULL, dev->irq);
    virt_irqs[1] = sparc_config.build_device_irq(NULL, dev->irq + 1);
    dev->base_addr = (RAMP_ENET_ADDRMASK + dev_id) << RAMP_IOADDRMASK_SHIFT;
    dev->mtu = RAMP_ENET_DEFAULT_MTU;
}


static int __init ramp_enet_init(void)
{
    unsigned int idx;
    struct net_device *dev;
    struct ramp_enet_private *tp;
    int ret;

    for (idx=0;idx<MAX_DEVS;idx++) {
        dev = alloc_etherdev(sizeof (struct ramp_enet_private));
        if (dev == NULL) {
            pr_emerg("ramp_enet: Unable to alloc new net device \n");
            return -ENODEV; 
        }
        tp = netdev_priv(dev);
        tp->dev = dev;

        //update the dev_list pointer
        if (idx != 0) 
            tp->prev_dev = dev_list;
        else
            tp->prev_dev = 0;

        dev_list = dev; 

        spin_lock_init(&tp->nfree_lock);
        spin_lock_init(&tp->cleanup_lock);
        tp->tx_irq_mask = 2;              //default state of tx_irq 
        tp->tx_cleanup_busy = 0;

        //init device   
        //default MTU
        tp->mtu_a = (RAMP_ENET_DEFAULT_MTU + RX_SKB_ALIGN - 1) & ~(RX_SKB_ALIGN - 1);

        ramp_enet_init_devs(dev, idx);          

        netif_napi_add(dev, &tp->napi, ramp_enet_poll, RAMP_NAPI_WEIGHT);

        ret = register_netdev(dev);
        if (ret) {
            printk ("ramp_enet: Error %d initializing RAMP Ethernet card #%d",ret, idx);
            free_netdev(dev);       
            return ret;     
        }
    }
    return 0;
}

static void __exit ramp_enet_cleanup(void)
{
    struct ramp_enet_private *tp;
    struct net_device *td;
    while (dev_list) {
        tp = netdev_priv(dev_list);     
        td = tp->prev_dev;

        unregister_netdev(dev_list);
        free_netdev(dev_list);  

        dev_list = td;
    }
}

module_init(ramp_enet_init);
module_exit(ramp_enet_cleanup);
