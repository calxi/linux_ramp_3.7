/*
 *  linux/arch/sparc/mm/ramp_mm.c
 *
 * Copyright (C) 2011 Zhangxi Tan (xtan@cs.berkeley.edu) 
 *
 * srmmu flush routines for ramp gold. The key is to flush the tiny I$.
 *
 */

#include <linux/kernel.h>
#include <linux/mm.h>
#include <asm/asi.h>
#include <asm/cacheflush_32.h>
#include <asm/ramp.h>
#include <asm/tlbflush.h>

#define FLUSH_BEGIN(mm) if((mm)->context != NO_CONTEXT) {
#define FLUSH_END       }

//This is the most severe flush
void ramp_flush_cache_all(void) 
{
        unsigned long faddr;
//	printk("flush_cache_all called\n");        

	unsigned long flags;
	local_irq_save(flags);
	flush_user_windows();
	local_irq_restore(flags);

        //use the MMU bypass flush to flush all 64K host cache 
        faddr = 0x10000 - 0x100;
	
        goto inside;
        do {
               faddr -= 0x100; 
        inside:
               __asm__ __volatile__("sta %%g0, [%1] %0\n\t"
                                    "sta %%g0, [%1 + %2] %0\n\t"
                                    "sta %%g0, [%1 + %3] %0\n\t"
                                    "sta %%g0, [%1 + %4] %0\n\t"
                                    "sta %%g0, [%1 + %5] %0\n\t"
                                    "sta %%g0, [%1 + %6] %0\n\t"
                                    "sta %%g0, [%1 + %7] %0\n\t"
                                    "sta %%g0, [%1 + %8] %0\n\t" : :                              
                                    "i"(ASI_M_FLUSH_BYPASS), "r" (faddr), "r" (0x20), "r" (0x40), 
                                    "r" (0x60), "r" (0x80), "r" (0xa0), "r"(0xc0), "r"(0xe0)
                                    : "memory");	
        }while(faddr);
}

//flush D$ using virtual addresses
static inline void __ramp_flush_dcache_page(unsigned long page)
{
//     		ramp_flush_cache_all();
//#if 0
                unsigned long line/*, faddr*/;
//		printk("flush_dcache_page called\n");

                page &= PAGE_MASK;
                line = (page + PAGE_SIZE) - 0x100;
                goto inside;
                do {
	                line -= 0x100;
               	inside:
/*		faddr = line & 0xffff;
               __asm__ __volatile__("sta %%g0, [%1] %0\n\t"
                                    "sta %%g0, [%1 + %2] %0\n\t"
                                    "sta %%g0, [%1 + %3] %0\n\t"
                                    "sta %%g0, [%1 + %4] %0\n\t"
                                    "sta %%g0, [%1 + %5] %0\n\t"
                                    "sta %%g0, [%1 + %6] %0\n\t"
                                    "sta %%g0, [%1 + %7] %0\n\t"
                                    "sta %%g0, [%1 + %8] %0\n\t" : :                              
                                    "i"(ASI_M_FLUSH_BYPASS), "r" (faddr), "r" (0x20), "r" (0x40), 
                                    "r" (0x60), "r" (0x80), "r" (0xa0), "r"(0xc0), "r"(0xe0)
                                    : "memory");*/
 
                __asm__ __volatile__("sta %%g0, [%1] %0\n\t"
                                    "sta %%g0, [%1 + %2] %0\n\t"
                                    "sta %%g0, [%1 + %3] %0\n\t"
                                    "sta %%g0, [%1 + %4] %0\n\t"
                                    "sta %%g0, [%1 + %5] %0\n\t"
                                    "sta %%g0, [%1 + %6] %0\n\t"
                                    "sta %%g0, [%1 + %7] %0\n\t"
                                    "sta %%g0, [%1 + %8] %0\n\t" : :                              
                                    "i"(ASI_M_DFLUSH), "r" (line), "r" (0x20), "r" (0x40), 
                                    "r" (0x60), "r" (0x80), "r" (0xa0), "r"(0xc0), "r"(0xe0)
                                    : "memory");
       	        } while(line != page);        
//#endif
}

//flush I/D $ pages
static inline void __ramp_flush_icache_page(unsigned long page)
{
// 	ramp_flush_cache_all();
//#if 0
        unsigned long line;

// 	unsigned int pc, ro7;
//	printk("flush_icache_page called\n");

        page &= PAGE_MASK;
        line = (page + PAGE_SIZE) - 0x100;
//	printk("line = %lx, page = %lx \n", line, page);
        goto inside;
        do {
                line -= 0x100;
               	inside:
/*               __asm__ __volatile__ ("mov %%o7, %1; 1: call 2f; nop; 2: mov %%o7,%0; mov %1, %%o7" : "=r"(pc), "=r"(ro7) : : "o7");
	       printk("pc = %x\n", pc);
               __asm__ __volatile__("flush %0 \n\t" :: "r" (line): "memory");
	        printk("first flush ok\n");			
               __asm__ __volatile__("flush %0 + %1 \n\t"
                                   "flush %0 + %2 \n\t"
                                   "flush %0 + %3 \n\t"
                                   "flush %0 + %4 \n\t"
                                   "flush %0 + %5 \n\t"
                                   "flush %0 + %6 \n\t"
                                   "flush %0 + %7 \n\t" : :                              
                                   "r" (line), "i" (0x20), "i" (0x40), 
                                   "i" (0x60), "i" (0x80), "i" (0xa0), "i"(0xc0), "i"(0xe0)
                                   : "memory");*/
                __asm__ __volatile__("flush %0 \n\t"
                                   "flush %0 + %1 \n\t"
                                   "flush %0 + %2 \n\t"
                                   "flush %0 + %3 \n\t"
                                   "flush %0 + %4 \n\t"
                                   "flush %0 + %5 \n\t"
                                   "flush %0 + %6 \n\t"
                                   "flush %0 + %7 \n\t" : :                              
                                   "r" (line), "i" (0x20), "i" (0x40), 
                                   "i" (0x60), "i" (0x80), "i" (0xa0), "i"(0xc0), "i"(0xe0)
                                   : "memory");
      } while(line != page);        	
//#endif
}

//used by kernel when loading user binaries, so need to flush D$ as well
void ramp_flush_icache_range(unsigned long start, unsigned long end)
{
	unsigned long flags;
//	printk("flush_icache_range called\n");
//	flush_user_windows();
	local_irq_save(flags);
	flush_user_windows();

//	ramp_flush_cache_all();
//#if 0
	start &= PAGE_MASK;     //start page
        end   &= PAGE_MASK;     //end page
	do {
                __ramp_flush_icache_page(start);
        	start += PAGE_SIZE;
	}while(start <= end);
//#endif
	local_irq_restore(flags);
}

void ramp_flush_page_to_ram(unsigned long page)
{
//	ramp_flush_cache_all();
//#if 0
        unsigned long flags;
//	printk("flush_page_to_ram called\n");
	local_irq_save(flags);  //turn off interrupt
        flush_user_windows();
        __ramp_flush_dcache_page(page);
//	ramp_flush_cache_all();
        local_irq_restore(flags);
//#endif
} 

void ramp_flush_icache_all(void)
{
//	ramp_flush_cache_all();
//#if 0
	unsigned long paddr = 0;
//	printk("flush_icache_all called\n");
	//we only have 8 lines so flush all
        __asm__ __volatile__("sta %%g0, [%1] %0\n\t"
                             "sta %%g0, [%1 + %2] %0\n\t"
                             "sta %%g0, [%1 + %3] %0\n\t"
                             "sta %%g0, [%1 + %4] %0\n\t"
                             "sta %%g0, [%1 + %5] %0\n\t"
                             "sta %%g0, [%1 + %6] %0\n\t"
                             "sta %%g0, [%1 + %7] %0\n\t"
                             "sta %%g0, [%1 + %8] %0\n\t" : :                              
                             "i"(ASI_M_FLUSH_BYPASS), "r" (paddr | 0x80000000), "r" (0x20), "r" (0x40), 
                             "r" (0x60), "r" (0x80), "r" (0xa0), "r"(0xc0), "r"(0xe0)
                              : "memory"); 
//#endif
}

// I want use the 3rd argument pfn here!! It was dropped by asm/flushcache_32.h
void ramp_flush_cache_page(struct vm_area_struct *vma, unsigned long page)
{
//	ramp_flush_cache_all();
//#if 0
	struct mm_struct *mm = vma->vm_mm;
	unsigned long flags;
	int octx;

//	printk("flush_cache_page called\n");
	FLUSH_BEGIN(mm)
        local_irq_save(flags);  //turn off interrupt
        flush_user_windows();
        octx = srmmu_get_context();
        srmmu_set_context(mm->context);
        
        if (vma->vm_flags & VM_EXEC)    //avoid flush insturction 
           __ramp_flush_icache_page(page); 	
        else   //flush d$ only, gcc inlining can take care of optimizations
           __ramp_flush_dcache_page(page);
       
        srmmu_set_context(octx);
        local_irq_restore(flags);
	FLUSH_END
//#endif
}

void ramp_flush_sig_insns(struct mm_struct *mm, unsigned long insn_addr)
{
//	ramp_flush_cache_all();
//#if 0
       unsigned long flags;
//	printk("flush_sig_insns called\n");


       FLUSH_BEGIN(mm)          //no need to flush the user window (as writing from kernel to user)
       local_irq_save(flags);
       __asm__ __volatile__("flush %0+%%g0\n\t" : : "r" (insn_addr) : "memory");
       local_irq_restore(flags);
       FLUSH_END
//#endif 
}

void ramp_flush_cache_range(struct vm_area_struct *vma, unsigned long start, unsigned long end)
{
//	ramp_flush_cache_all();
//#if 0
	struct mm_struct *mm = vma->vm_mm;
	unsigned long flags;
	int octx;
//	printk("flush_cache_range called, start=%lx end=%lx\n", start, end);

	FLUSH_BEGIN(mm)
	local_irq_save(flags);
	flush_user_windows();
	octx = srmmu_get_context();
	srmmu_set_context(mm->context);

	start &= PAGE_MASK;     //start page
        end   &= PAGE_MASK;     //end page
//	printk("end = %lx\n", end);
	do {
//		printk("start = %lx\n", start);
                __ramp_flush_icache_page(start);
        	start += PAGE_SIZE;		
//		printk("new_start = %lx\n", start);
	}while(start <= end);
	srmmu_set_context(octx);
	local_irq_restore(flags);
	FLUSH_END
//#endif
}

void ramp_flush_page_for_dma(unsigned long page)
{
//	ramp_flush_cache_all();
//#if 0
	unsigned long flags;
//	printk("flush_page_for_dma called\n");

	local_irq_save(flags);	//prevent from context switch
	__ramp_flush_dcache_page(page);
	local_irq_restore(flags);
	 __asm__ __volatile__("stbar \n\t" : : : "memory");	//make sure to flush to ram	
//#endif
}

void ramp_flush_tlb_all()
{
//	printk("flush tlb all\n");
	srmmu_flush_whole_tlb();
}

void ramp_flush_tlb_mm(struct mm_struct *mm)
{
//	ramp_flush_tlb_all();
//#if 0 
	unsigned long flags;
//	printk("flush tlb mm\n");

	FLUSH_BEGIN(mm)
	local_irq_save(flags);
	__asm__ __volatile__(
	"lda	[%0] %3, %%g5\n\t"
	"sta	%2, [%0] %3\n\t"
	"sta	%%g0, [%1] %4\n\t"
	"sta	%%g5, [%0] %3\n"
	: /* no outputs */
	: "r" (SRMMU_CTX_REG), "r" (0x300), "r" (mm->context),
	  "i" (ASI_M_MMUREGS), "i" (ASI_M_FLUSH_PROBE)
	: "g5");
	local_irq_restore(flags);
	FLUSH_END
//#endif
}

void ramp_flush_tlb_range(struct vm_area_struct *vma, unsigned long start, unsigned long end)
{
//      ramp_flush_tlb_all();

//#if 0
	struct mm_struct *mm = vma->vm_mm;
	unsigned long size, flags;
//	printk("flush_tlb_range\n");

	FLUSH_BEGIN(mm)
	start &= SRMMU_PGDIR_MASK;
	size = SRMMU_PGDIR_ALIGN(end) - start;
	local_irq_save(flags);
	__asm__ __volatile__(
		"lda	[%0] %5, %%g5\n\t"
		"sta	%1, [%0] %5\n"
		"1:\n\t"
		"subcc	%3, %4, %3\n\t"
		"bne	1b\n\t"
		" sta	%%g0, [%2 + %3] %6\n\t"
		"sta	%%g5, [%0] %5\n"
	: /* no outputs */
	: "r" (SRMMU_CTX_REG), "r" (mm->context), "r" (start | 0x200),
	  "r" (size), "r" (SRMMU_PGDIR_SIZE), "i" (ASI_M_MMUREGS),
	  "i" (ASI_M_FLUSH_PROBE)
	: "g5", "cc");
	local_irq_restore(flags);
	FLUSH_END
//#endif
}

void ramp_flush_tlb_page(struct vm_area_struct *vma, unsigned long page)
{
//      ramp_flush_tlb_all();

//#if 0
	struct mm_struct *mm = vma->vm_mm;
	unsigned long flags;
//	printk("flush_tlb_page called\n");


	FLUSH_BEGIN(mm)
	local_irq_save(flags);
	__asm__ __volatile__(
	"lda	[%0] %3, %%g5\n\t"
	"sta	%1, [%0] %3\n\t"
	"sta	%%g0, [%2] %4\n\t"
	"sta	%%g5, [%0] %3\n"
	: /* no outputs */
	: "r" (SRMMU_CTX_REG), "r" (mm->context), "r" (page & PAGE_MASK),
	  "i" (ASI_M_MMUREGS), "i" (ASI_M_FLUSH_PROBE)
	: "g5");
	local_irq_restore(flags);
	FLUSH_END
//#endif 
}


