/*
 * Copyright (C) 2011 Zhangxi Tan (xtan@cs.berkeley.edu) UC Berkeley
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/mutex.h>
#include <linux/interrupt.h>

#include <asm/timer.h>
#include <asm/traps.h>
#include <asm/cacheflush.h>
#include <asm/ramp.h>

#include "irq.h"

extern struct irq_bucket irq_table[NR_IRQS];

static unsigned long ramp_int_mask;     //single core interrupt mask
const unsigned long ramp_int_mask_shift [] = {0, 0, 0, 0, 0, 1, 2 ,3 ,4 ,5 ,0, 0, 0, 0, 0};

inline void ramp_enable_irq(unsigned int irq_nr)
{
    unsigned long flags, addr;
    addr = RAMP_IRQMASK_ADDR | (ramp_cpu_id() << 10);       

    local_irq_save(flags);

    ramp_int_mask |= 1UL << (ramp_int_mask_shift[irq_nr & 0xF]);

    ramp_store_reg(addr, ramp_int_mask);
    local_irq_restore(flags);
}

inline void ramp_disable_irq(unsigned int irq_nr)
{
    unsigned long addr, flags;
    addr = RAMP_IRQMASK_ADDR | (ramp_cpu_id() << 10);       

    local_irq_save(flags);
    ramp_int_mask &= ~(1UL << ramp_int_mask_shift[irq_nr & 0xF]);
    ramp_store_reg(addr, ramp_int_mask);
    local_irq_restore(flags);
}

static void ramp_mask_irq(struct irq_data *data)
{
    int real_irq = irq_table[data->irq].real_irq;
    // printk("ramp_mask_irq #: %x, real: %x\n", data->irq, real_irq);
    ramp_disable_irq(real_irq);
}

static void ramp_unmask_irq(struct irq_data *data)
{
    int real_irq = irq_table[data->irq].real_irq;
    // printk("ramp_unmask_irq #: %x, real: %x\n", data->irq, real_irq);
    ramp_enable_irq(real_irq);
}

static unsigned int ramp_startup_irq(struct irq_data *data)
{
    irq_link(data->irq);
    ramp_unmask_irq(data);

    return 0;
}

static void ramp_shutdown_irq(struct irq_data *data)
{
    ramp_mask_irq(data);
    irq_unlink(data->irq);
}

static struct irq_chip ramp_irq_chip = {
	.name			= "ramp gold",
	.irq_startup		= ramp_startup_irq,
	.irq_shutdown		= ramp_shutdown_irq,
	.irq_mask		= ramp_mask_irq,
	.irq_unmask		= ramp_unmask_irq,
};

void ramp_clear_clock_irq(void)
{
}

void ramp_load_profile_irq(int cpu, unsigned int limit)
{
    // BUG();
}

unsigned int ramp_build_device_irq(unsigned int real_irq,
        irq_flow_handler_t flow_handler,
        const char *name)
{
    unsigned int irq;

    irq = irq_alloc(real_irq, real_irq);

    // printk("ramp_build_device_irq real: %x, virt: %x", real_irq, irq);

    if (irq != 0) {
        irq_set_chip_and_handler_name(irq, &ramp_irq_chip, flow_handler, name);
    }

    return irq;
}

static unsigned int _ramp_build_device_irq(struct platform_device *op,
        unsigned int real_irq)
{
    return ramp_build_device_irq(real_irq, handle_simple_irq, "edge");
}

static u32 ramp_cycles_offset(void) {
    return ramp_load_reg(RAMP_TIMER_ADDR);
}

static u32 ramp_timer_period(void) {
    int timer_period, len;
    struct device_node *rootnp, *np;
    struct property *pp;

    timer_period = 1<<20; // must be power of 2 

    rootnp = of_find_node_by_path("/"); 

    if (rootnp) { 	
        np = of_find_node_by_name(rootnp, "ramp_cpu");
        if (np) {
            pp = of_find_property(np, "timer_period", &len);
            if (pp) 
                timer_period = *((unsigned int*)(pp->value));		
        }
    }

    return timer_period;
}

void __init ramp_init_timers(void) // irq_handler_t counter_fn)
{
    int irq, err, timer_period, timer_en, timer_data;

    sparc_config.get_cycles_offset = ramp_cycles_offset;
    sparc_config.features |= FEAT_L10_CLOCKSOURCE;

    irq = _ramp_build_device_irq(NULL, RAMP_TIMER_INTNO);
    err = request_irq(irq,
            timer_interrupt,
            (IRQF_DISABLED | SA_STATIC_ALLOC),
            "timer",
            NULL
        );

    if (err) {
        printk("ramp_time_init: unable to attach IRQ%d\n",
                RAMP_TIMER_INTNO);
        prom_halt();
    }

    timer_period = ramp_timer_period();

    printk("Timer period:%u\n", timer_period);
    sparc_config.cs_period = timer_period;

    timer_en = 1<<24; // don't change this
    timer_data = timer_en | (timer_period-1);
    ramp_store_reg(RAMP_TIMER_ADDR, timer_data);
}

void __init ramp_init_IRQ(void)
{
    sparc_config.init_timers = ramp_init_timers;
    sparc_config.build_device_irq = _ramp_build_device_irq;
    // will be overriden by init_timers
    sparc_config.clock_rate = ramp_timer_period() * RAMP_TIMER_FREQ;
    printk("=== clock_rate: %i", sparc_config.clock_rate);
    sparc_config.clear_clock_irq = ramp_clear_clock_irq;
    sparc_config.load_profile_irq = ramp_load_profile_irq;

    /* old kernel stuff
       sparc_irq_config.init_timers = ramp_init_timers;
       ramp_int_mask = 0;

       BTFIXUPSET_CALL(enable_irq, ramp_enable_irq, BTFIXUPCALL_NORM);
       BTFIXUPSET_CALL(disable_irq, ramp_disable_irq, BTFIXUPCALL_NORM);
       BTFIXUPSET_CALL(enable_pil_irq, ramp_enable_irq, BTFIXUPCALL_NORM);
       BTFIXUPSET_CALL(disable_pil_irq, ramp_disable_irq, BTFIXUPCALL_NORM);

       BTFIXUPSET_CALL(clear_clock_irq, ramp_clear_clock_irq,
       BTFIXUPCALL_NORM);
       BTFIXUPSET_CALL(load_profile_irq, ramp_load_profile_irq,
       BTFIXUPCALL_NOP);
       */
}

