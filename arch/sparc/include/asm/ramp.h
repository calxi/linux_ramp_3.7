/*
 * ramp.h: RAMP Gold specific definitions and defines.
 *
 * Copyright (C) 2011 Zhangxi Tan (xtan@cs.berkeley.edu)
 */

#ifndef _SPARC_RAMP_H
#define _SPARC_RAMP_H

#include <linux/mm.h>           /* Common for other includes */

//flush functions
extern void ramp_flush_cache_all(void);
extern void ramp_flush_cache_range(struct vm_area_struct *vma, unsigned long start,
				     unsigned long end);
extern void ramp_flush_cache_page(struct vm_area_struct *vma, unsigned long page);
extern void ramp_flush_page_to_ram(unsigned long page);
extern void ramp_flush_page_for_dma(unsigned long page);
extern void ramp_flush_sig_insns(struct mm_struct *mm, unsigned long addr);
extern void ramp_flush_tlb_all(void);
extern void ramp_flush_tlb_mm(struct mm_struct *mm);
extern void ramp_flush_tlb_range(struct vm_area_struct *vma, unsigned long start,
				   unsigned long end);
extern void ramp_flush_tlb_page(struct vm_area_struct *vma,
				  unsigned long page);
extern void ramp_flush_icache_all(void);
extern void ramp_flush_icache_range(unsigned long start, unsigned long end);

//init functions at boot
extern void ramp_init_IRQ(void);

//timer functions
extern u32 ramp_do_gettimeoffset(void);


//IO bus functions
static inline unsigned long ramp_load_reg(unsigned long paddr)
{
	unsigned long retval;
	__asm__ __volatile__("lda [%1] %2, %0\n\t" :
			     "=r"(retval) : "r"(paddr), "i"(ASI_M_IOBUS));
	return retval;
}

static inline u64 ramp_load_double_reg(unsigned long paddr)
{
	u64 retval;
	__asm__ __volatile__("ldda [%1] %2, %0\n\t" :
			     "=r"(retval) : "r"(paddr), "i"(ASI_M_IOBUS));
	return retval;

}

static inline void ramp_store_reg(unsigned long paddr, unsigned long value)
{
	__asm__ __volatile__("sta %0, [%1] %2\n\t" : : "r"(value), "r"(paddr),
			     "i"(ASI_M_IOBUS) : "memory");
}

//get core IDs
static inline unsigned long ramp_cpu_id(void)
{
	unsigned long id;

	asm volatile("mov %%asr15,%0" : "=&r"(id) : );
	return id & 0xff;
}


#define RAMP_IOADDRMASK_SHIFT		16

#define RAMP_KEYBOARD_INTNO		5
#define RAMP_TIMER_INTNO 		10
#define RAMP_ENET_INTNO			6 	//eth0 rx, 7 eth0 tx, 8 eth1 rx 9 eth1 tx

#define RAMP_TIMER_ADDR			(1UL << RAMP_IOADDRMASK_SHIFT)
#define RAMP_IRQMASK_ADDR		((2UL << RAMP_IOADDRMASK_SHIFT) | 4)
#define	RAMP_KEYBOARD_ADDR		(3UL << RAMP_IOADDRMASK_SHIFT)

#define RAMP_ENET_ADDRMASK		 4UL

#define RAMP_TIMER_FREQ         1024
#endif /* !(_SPARC_RAMP_H) */
